import React from "react";
import Tree from "../Tree";

const treeData=[
    {
        key:'0',
        label:"Documents",
        icon:"fa fa-folder",
        title:"Documents Folder",
        children:[
            {
                key:'0-0',
        label:"Documents 1-1",
        icon:"fa fa-folder",
        title:"Documents Folder"

            },
            {
                key:'0-1-1',
        label:"Documents 0-1",
        icon:"fa fa-file",
        title:"Documents Folder",
            },
            {
                key:'0-1-2',
        label:"Documents-0-2.doc",
        icon:"fa fa-file",
        title:"Documents Folder",
            },
            {
                key:'0-1-3',
        label:"Documents-0-3.doc",
        icon:"fa fa-file",
        title:"Documents Folder",
            }
        ]
    },    
        
            {
                key:'1',
                label:"Documents",
                icon:"fa fa-folder",
                title:"Documents Folder",
                children:[
                    {
                        key:'1-0',
                label:"Documents 1-1",
                icon:"fa fa-folder",
                title:"Documents Folder"
        
                    },
                    {
                        key:'1-1-1',
                label:"Documents 1-1",
                icon:"fa fa-file",
                title:"Documents Folder",
                    },
                    {
                        key:'1-1-2',
                label:"Documents-1-2.doc",
                icon:"fa fa-file",
                title:"Documents Folder",
                    },
                    {
                        key:'1-1-3',
                label:"Documents-1-3.doc",
                icon:"fa fa-file",
                title:"Documents Folder",
                    }
                ]


    },
    {
        key:'2',
        label:"Documents",
        icon:"fa fa-folder",
        title:"Documents Folder",
        children:[
            {
                key:'2-0-0',
        label:"Documents 2-0",
        icon:"fa fa-folder",
        title:"Documents Folder"

            },
            {
                key:'2-1-1',
        label:"Documents 2-1",
        icon:"fa fa-file",
        title:"Documents Folder",
            },
            {
                key:'2-1-2',
        label:"Documents-2-2.doc",
        icon:"fa fa-file",
        title:"Documents Folder",
            },
            {
                key:'2-1-3',
        label:"Documents-2-3.doc",
        icon:"fa fa-file",
        title:"Documents Folder",
            }

        ]
    }
];


const TreeList=()=>{
    return(
        <>
        <div className="row">
            <div className="col text-center">
                <h2>
                    Tree Visulization component
                </h2>
                <p className="mt-3">
                    <div className="row mt-5 d-flex justify-content-center">
                        <div className="col-lg-8 text-center text-dark">
                            <Tree data={treeData}/>


                        </div>

                    </div>
                </p>

            </div>

        </div>
        </>
    );
};
export default TreeList;